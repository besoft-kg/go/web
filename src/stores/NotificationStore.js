import {types as t} from 'mobx-state-tree';
import Notification from '../models/Notification';
import {getCustomError, requester} from '../utils';

export default t
  .model('NotificationStore', {

    items: t.map(Notification),
    unread_items_count: t.optional(t.integer, 0),
    is_fetching: t.optional(t.boolean, false),

  })
  .actions(self => {

    function setValue(name, value) {
      self[name] = value;
    }

    function setItems(data) {
      self.items.clear();

      data.map(v => {
        self.items.set(v.id, Notification.create({
          ...v,
          read_on: v.read_on ? new Date(v.read_on) : null,
          created_at: new Date(v.created_at),
        }));
      });
    }

    function fetchItems() {
      if (self.is_fetching) {
        return;
      }
      setValue('is_fetching', true);
      requester.get(`/notification`).then(({data}) => {
        self.setItems(data.payload.data);
        const unread_items_count = self.unread_items_count - data.payload.data.filter(v => v.read_on === null).length;
        if (unread_items_count < 0) self.setValue('unread_items_count', 0);
        else self.setValue('unread_items_count', unread_items_count);
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      }).finally(() => {
        self.setValue('is_fetching', false);
      });
    }

    function clear() {
      self.items = {};
      self.unread_items_count = 0;
    }

    function checkUnreadItemsCount() {
      requester.get(`/notification/count`).then(({data}) => {
        self.setValue('unread_items_count', data.payload.unread_items_count);
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      });
    }

    return {
      setValue,
      setItems,
      fetchItems,
      clear,
      checkUnreadItemsCount,
    };

  })
  .views(self => ({}));
