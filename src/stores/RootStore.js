import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import PlaceStore from './PlaceStore';
import UserStore from './UserStore';
import TripStore from './TripStore';

export default t
  .model('RootStore', {

    appStore: AppStore,
    placeStore: PlaceStore,
    userStore: UserStore,
    tripStore: TripStore,

  })
  .actions(self => ({

    setDataFromStorage(values) {
      self.appStore.setLanguage(values[0], false);

      if (values[1]) self.appStore.token = values[1];

      if (values[2]) self.userStore.setItem(values[2]);
      self.appStore.user = values[2] ? self.userStore.items.get(values[2].id) : null;

      if (values[3]) self.placeStore.setItems(values[3], false);

      if (values[4]) self.appStore.fcm_token = values[4];

      self.appStore.app_is_ready = true;
    },

  }))
  .create({

    appStore: AppStore.create({
      authenticating: false,
      checking: false,
      app_is_ready: false,
    }),
    placeStore: PlaceStore.create({items: []}),
    userStore: UserStore.create({items: {}}),
    tripStore: TripStore.create({items: {}}),

  });
