import {getRoot, types as t} from 'mobx-state-tree';
import Request from '../models/Request';

export default t
  .model('RequestStore', {

    items: t.map(Request),

  })
  .actions(self => {

    function setValue(name, value) {
      self[name] = value;
    }

    function setItem(data) {
      self.items.set(data.id, Request.create({
        ...data,
        passenger: data.passenger_id,
        driver: data.driver_id,
        created_at: new Date(data.created_at),
      }));
    }

    function setItems(data) {
      data.map(v => {
        self.items.set(v.id, Request.create({
          ...v,
          passenger: v.passenger_id,
          driver: v.driver_id,
          created_at: new Date(v.created_at),
        }));
      });
    }

    function clear() {
      self.items = {};
    }

    return {
      setValue,
      setItems,
      setItem,
      clear,
    };

  })
  .views(self => ({

    get rootStore() {
      return getRoot(self);
    },

    get appStore() {
      return self.rootStore.appStore;
    },

    get pending_items() {
      return Array.from(self.items.values()).filter(v => v.sender !== self.appStore.user.type && v.status === 'pending');
    },

    get first_voyage() {
      if (self.pending_items.length < 1) {
        return null;
      }
      return self.pending_items[0][self.appStore.user.type === 'driver' ? 'offer_id' : 'trip_id'];
    },

    get new_items_card() {

      if (self.pending_items.length < 1) {
        return null;
      }

      const {language} = self.appStore;

      //return (
      //   <Card style={{marginHorizontal: size(4), marginTop: size(4)}}>
      //     <List.Item
      //       style={{backgroundColor: Colors.blue700}}
      //       title={language === 'ru' ? `У вас есть новые запросы (+${self.pending_items.length})` : `Жаңы билдирмелер бар (+${self.pending_items.length})`}
      //       titleStyle={{color: Colors.white}}
      //       onPress={() => RootNavigator.navigate('Requests', {item_id: self.first_voyage})}
      //       left={props => <List.Icon {...props} color={Colors.white} icon="help-outline"/>}
      //       right={props => <List.Icon {...props} color={Colors.white} icon="keyboard-arrow-right"/>}
      //     />
      //   </Card>
      // );

      return null;
    },

  }));
