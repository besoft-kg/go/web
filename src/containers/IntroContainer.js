import React from 'react';
import Button from "@material-ui/core/Button";

class IntroContainer extends React.Component{
  render() {
    return (
      <div className="App">
        <Button variant="contained" color="primary">
          Intro
        </Button>
      </div>
    );
  }
}

export default IntroContainer;
