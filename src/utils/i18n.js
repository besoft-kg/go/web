import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {DEBUG_MODE} from "./settings";

import translationKy from '../locales/ky.json';
import translationRu from '../locales/ru.json';
import storage from "./storage";

i18n
  .use(initReactI18next)
  // for all options read: https://www.i18next.com/overview/configuration-options
  .init({
    fallbackLng: ['ru', 'ky'],
    debug: DEBUG_MODE,
    defaultNS: 'common',
    ns: [
      'common',
      'home',
      'profile',
      'settings',
      'welcome',
      'notifications',
      'history',
    ],
    resources: {
      ky: translationKy,
      ru: translationRu,
    },
    load: 'all',
    interpolation: {
      escapeValue: false,
    },
    react: {
      wait: true,
      useSuspense: true,
    },
  });

export default i18n;
