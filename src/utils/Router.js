import React from "react";
import {
  BrowserRouter,
  Route,
} from "react-router-dom";
import IntroContainer from "../containers/IntroContainer";
import AuthContainer from "../containers/AuthContainer";

class Router extends React.Component {
  render() {
    return (
      <BrowserRouter>
        {/*<Route path="/" exact={true}>*/}
        {/*  <IntroContainer/>*/}
        {/*</Route>*/}
        <Route path="/" exact={true}>
          <AuthContainer/>
        </Route>
      </BrowserRouter>
    );
  }
}

export default Router;
