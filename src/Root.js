import React from 'react';
import BaseComponent from "./components/BaseComponent";
import App from "./App";
import {Provider} from 'mobx-react';
//import RootStore from "./stores/RootStore";

//const intro_is_skipped = localStorage.getItem('intro_is_skipped');

//if (!intro_is_skipped) window.location.href = '/introduction.html';

class Root extends BaseComponent {
  render() {
    return (
      <App/>
    );
  }
}

export default Root;
