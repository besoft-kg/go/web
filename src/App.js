import React from 'react';
import './App.css';
import Router from "./utils/Router";

class App extends React.Component{
  render() {
    return (
      <Router />
    );
  }
}

export default App;
