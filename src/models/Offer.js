import {getRoot, types as t} from 'mobx-state-tree';
import User from './User';
import Place from './Place';
import {values} from 'mobx';
import moment from 'moment';

export default t
  .model('Offer', {

    id: t.identifierNumber,
    user_id: t.integer,
    user: t.reference(User),
    from_id: t.integer,
    from: t.reference(Place),
    to_id: t.integer,
    to: t.reference(Place),
    persons: t.integer,
    datetime: t.Date,
    note: t.maybeNull(t.string),
    created_at: t.Date,

    _loading: t.optional(t.boolean, false),

  })
  .views(self => ({

    get rootStore() {
      return getRoot(self);
    },

    get requests() {
      return Array.from(values(self.rootStore.requestStore.items)).filter(v => v.offer_id === self.id);
    },

    get trips() {
      return Array.from(values(self.rootStore.tripStore.items)).filter(v => v.from_id === self.from_id && v.to_id === self.to_id && v.datetime <= moment(self.datetime).add(5, 'hours') && v.datetime >= moment(self.datetime).subtract(5, 'hours'));
    },

    get request() {
      const {user} = getRoot(self).appStore;
      return this.requests.find(v => v.offer_id === self.id && v[`${user.type}_id`] === user.id);
    },

    get free_places_count() {
      return self.persons - self.booked_places_count;
    },

    get booked_places_count() {
      let total = 0;
      self.requests.filter(v => v.status === 'accepted').map(v => {
        total += self.rootStore.tripStore.items.get(v.trip_id).persons;
      });
      return total;
    },

    get is_past() {
      return self.datetime <= moment().subtract(5, 'hours');
    },

  }))
  .actions(self => ({

    setRequest(data) {
      self.rootStore.requestStore.setItem(data);
    },

    setValue(name, value) {
      self[name] = value;
    },

  }));
