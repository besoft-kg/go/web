import {getRoot, types as t} from 'mobx-state-tree';
import User from './User';
import Place from './Place';
import {values} from 'mobx';
import moment from 'moment';
import {getCustomError, requester} from '../utils';

export default t
  .model('Trip', {

    id: t.identifierNumber,
    user_id: t.integer,
    user: t.maybeNull(t.reference(User)),
    from_id: t.integer,
    from: t.reference(Place),
    to_id: t.integer,
    to: t.reference(Place),
    persons: t.integer,
    price: t.maybeNull(t.integer),
    datetime: t.Date,
    created_at: t.Date,
    //vehicle_type: t.maybeNull(t.enumeration(['passenger', 'minivan'])),
    note: t.maybeNull(t.string),
    contacts: t.maybeNull(t.frozen()),
    who: t.enumeration(['driver', 'passenger']),

    _loading: t.optional(t.boolean, false),

  })
  .views(self => ({

    get rootStore() {
      return getRoot(self);
    },

    get requests() {
      return Array.from(values(self.rootStore.requestStore.items)).filter(v => v.trip_id === self.id);
    },

    get offers() {
      return Array.from(values(self.rootStore.offerStore.items)).filter(v => v.from_id === self.from_id && v.to_id === self.to_id && v.datetime <= moment(self.datetime).add(5, 'hours') && v.datetime >= moment(self.datetime).subtract(5, 'hours'));
    },

    get request() {
      const {user} = self.rootStore.appStore;
      return this.requests.find(v => v[`${user.type}_id`] === user.id);
    },

    get persons_text() {
      if (self.rootStore.appStore.language === 'ky') return `${self.persons} бош орун`;
      if (self.persons === 1) return '1 свободное место';
      return `${self.persons} свободных мест`;
    },

    get is_past() {
      return self.datetime <= moment().subtract(5, 'hours');
    },

  }))
  .actions(self => ({

    setRequest(data) {
      getRoot(self).requestStore.setItem(data);
    },

    setValue(name, value) {
      self[name] = value;
    },

    delete() {
      if (self._loading) return;
      self._loading = true;
      requester.delete('/trip', {id: self.id}).then(({data}) => {
        if (data.result === 'success') {
          getRoot(self).tripStore.destroyItem(self);
        }
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      }).finally(() => {
        self.setValue('_loading', false);
      });
    },

  }));
