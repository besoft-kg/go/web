import {types as t} from 'mobx-state-tree';
import User from './User';

export default t
  .model('Request', {
    id: t.identifierNumber,
    passenger_id: t.integer,
    passenger: t.reference(User),
    trip_id: t.integer,
    offer_id: t.integer,
    driver_id: t.integer,
    driver: t.reference(User),
    sender: t.enumeration(['driver', 'passenger']),
    status: t.optional(t.enumeration(['pending', 'accepted', 'rejected', 'cancelled']), 'pending'),
    created_at: t.Date,
  })
  .actions(self => ({}));
