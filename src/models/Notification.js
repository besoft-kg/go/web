import {types as t} from 'mobx-state-tree';

const Notification = t
  .model('Notification', {
    id: t.identifierNumber,
    type: t.string,
    read_on: t.maybeNull(t.Date),
    data: t.maybeNull(t.frozen()),
    created_at: t.Date,
  })
  .views(self => ({

    get is_unread() {
      return self.read_on === null;
    },

  }))
  .actions(self => ({}));

export default Notification;
