import {getRoot, types as t} from 'mobx-state-tree';

const Place = t
  .model('Place', {
    id: t.identifierNumber,
    parent_id: t.maybeNull(t.integer),
    title_ru: t.string,
    title_ky: t.string,
  })
  .views(self => ({
    get title() {
      return self[`title_${getRoot(self).appStore.language}`];
    },
  }))
  .actions(self => ({}));

export default Place;
